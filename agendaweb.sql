CREATE DATABASE AgendaWeb;

USE AgendaWeb;

CREATE TABLE Usuario(
   idUsuario INT AUTO_INCREMENT NOT NULL,
   nick VARCHAR(30) NOT NULL,
   contrasena VARCHAR(30) NOT NULL,
   PRIMARY KEY(idUsuario)
);

CREATE TABLE Categoria(
  idCategoria INT AUTO_INCREMENT NOT NULL,
  nombreCategoria VARCHAR(30) NOT NULL,
  PRIMARY KEY(idCategoria)
);

CREATE TABLE Contacto(
  idContacto INT AUTO_INCREMENT NOT NULL,
  nombre VARCHAR(30) NOT NULL,
  apellido VARCHAR(30) NOT NULL,
  direccion VARCHAR(30) NOT NULL,
  telefono VARCHAR(12) NOT NULL,
  correo VARCHAR(40) NOT NULL,
  idCategoria INT NOT NULL,
  PRIMARY KEY(idContacto), 
  FOREIGN KEY(idCategoria) REFERENCES Categoria(idCategoria)	
  );


CREATE TABLE Historial(
   idHistorial INT AUTO_INCREMENT NOT NULL,
   idUsuario INT NOT NULL,
   fecha DATETIME NOT NULL,
   detalle VARCHAR(255) NOT NULL,
   PRIMARY KEY(idHistorial),
   FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario)	
);

CREATE TABLE UsuarioDetalle(
   idUsuarioDetalle INT AUTO_INCREMENT NOT NULL,
   idUsuario INT NOT NULL,
   idContacto INT NOT NULL,
   PRIMARY KEY(idUsuarioDetalle),
   FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario),
   FOREIGN KEY(idContacto) REFERENCES Contacto(idContacto)
);


-- PROCEDIMIENTOS ALMACENADOS

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ActualizarContacto` (IN `nombre1` VARCHAR(30), IN `apellido1` VARCHAR(30), IN `direccion1` VARCHAR(30), IN `telefono1` VARCHAR(12), IN `correo1` VARCHAR(40), IN `idCategoria1` INT, IN `idContacto1` INT, IN `idUsuario1` INT)  BEGIN
	insert into historial(idUsuario, fecha, detalle) values(idUsuario1, SYSDATE(), CONCAT("Se modifico el contacto ", (SELECT Concat(nombre," ",apellido) FROM contacto where idContacto = idContacto1), " por ", Concat(nombre1, " ", apellido1)));
    UPDATE Contacto SET nombre = nombre1, apellido = apellido1, direccion = direccion1, telefono = telefono1, correo = correo1, idCategoria = idCategoria1 WHERE idContacto = idContacto1;	
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_BorrarContacto` (IN `idContacto1` INT)  BEGIN
	DELETE FROM Contacto WHERE idContacto = idContacto1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_BorrarContactoUsuario` (IN `idUsuario1` INT, IN `idContacto1` INT)  BEGIN
	insert into historial(idUsuario, fecha, detalle) values(idUsuario1, SYSDATE(), CONCAT("Se elimino el contacto ", (SELECT Concat(nombre," ",apellido) FROM contacto where idContacto = idContacto1)));	
	DELETE FROM Contacto WHERE idContacto = idContacto1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_InsertarCategoria` (IN `nombre` VARCHAR(30))  BEGIN
	INSERT INTO Categoria(nombreCategoria) VALUES(nombre);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_InsertarContactoUsuario` (IN `nombre1` VARCHAR(30), IN `apellido1` VARCHAR(30), IN `direccion1` VARCHAR(30), IN `telefono1` VARCHAR(12), IN `correo1` VARCHAR(40), IN `idCategoria1` INT, IN `idUsuario1` INT)  BEGIN
	INSERT INTO contacto (nombre, apellido, direccion, telefono, correo, idCategoria) VALUES(nombre1, apellido1, direccion1, telefono1, correo1, idCategoria1);
    insert into usuarioDetalle(idUsuario, idContacto) values(idUsuario1, (SELECT idContacto from contacto where correo = correo1));
	insert into historial(idUsuario, fecha, detalle) values(idUsuario1, SYSDATE(), CONCAT("Se agrego el contacto ", (SELECT Concat(nombre," ",apellido) FROM contacto where correo = correo1)));
END$$

DELIMITER ;





CALL SP_InsertarCategoria('Familia');
CALL SP_InsertarCategoria('Amigos');
CALL SP_InsertarCategoria('Trabajo');
CALL SP_InsertarCategoria('Universidad');
CALL SP_InsertarCategoria('Escuela');
CALL SP_InsertarCategoria('Otros');


