var database = require('./database');
var categoria = {};
categoria.selectAll = function(callback) {
  if(database) {
    database.query("SELECT * FROM Categoria",
    function(error, result) {
      if(error) {
        throw error;
      } else {
        callback(null, result);
      }
    });
  }
}

categoria.select = function(idCategoria, callback) {
  if(database) {
    var consulta = "SELECT * FROM Categoria WHERE idCategoria = ?";
    database.query(consulta,idCategoria, function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });
  }
}




categoria.insert =function(data, callback){
  if(database){
    database.query("CALL SP_InsertarCategoria(?)", [data.nombreCategoria], function(error, resultado){
      if(error){
        throw error;
      } else {
        callback(null, {"insertId" : resultado.insertId});
      }
    });
  }
}


categoria.delete = function(idCategoria, callback){
  if(database){
    var sql = "DELETE FROM Categoria WHERE idCategoria = ?";
    database.query(sql, idCategoria, function(error, resultado){
      if(error){
        throw error;
      } else {
        var notificacion = {"Mensaje" : ""}
        if(resultado.affectedRows > 0){
            notificacion.Mensaje = "Se elimino la categoria";
        } else {
          notificacion.Mensaje = "No se elimino la categoria";
        }
        callback(null, notificacion);
      }
    });
  }
}



module.exports = categoria;
